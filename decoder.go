package trc20_transfer_decoder

import (
	hex2 "encoding/hex"
	"errors"
	"fmt"
	"github.com/fbsobreira/gotron-sdk/pkg/address"
	"math/big"
	"strings"
)

const trc20TransferMethodSignature = "0xa9059cbb"
const trc20TransferFromMethodSignature = "0x23b872dd"

type Data struct {
	Amount    int64
	ToAddress string
}

func IsTransfer(data string) bool {
	transferSignatures := []string{
		trc20TransferMethodSignature,
	}

	for _, methodSignature := range transferSignatures {
		startIndex := strings.Index("0x"+data, methodSignature)
		if startIndex != -1 {
			return true
		}
	}

	return false
}

func Decode(data string) (*Data, error) {
	bigAmount, err := decodeAmount(data)
	if err != nil {
		return nil, fmt.Errorf("can not decode amount from data: %s", err)
	}

	toAddress, err := getAddressFromReq(data)
	if err != nil {
		return nil, fmt.Errorf("can decode address from data: %s", err)
	}

	return &Data{
		Amount:    bigAmount.Int64(),
		ToAddress: toAddress,
	}, nil
}

func decodeAmount(data string) (*big.Int, error) {
	if len(data) <= 64 {
		return nil, errors.New("data does not supported")
	}
	amountHex := data[len(data)-64:]

	amountBytes, err := hex2.DecodeString(amountHex)
	if err != nil {
		return nil, err
	}

	decodedAmount := new(big.Int).SetBytes(amountBytes)

	return decodedAmount, nil
}

func getAddressFromReq(data string) (string, error) {
	supportedMethodSignatures := []string{
		trc20TransferMethodSignature,
	}

	for _, methodSignature := range supportedMethodSignatures {
		startIndex := strings.Index("0x"+data, methodSignature)
		if startIndex == -1 {
			continue
		}

		if methodSignature == trc20TransferMethodSignature {
			return getAddressFromTrc20TransferMethodSignature(startIndex, data)
		}

	}

	return "", fmt.Errorf("methodSignature is not supported")
}

func getAddressFromTrc20TransferMethodSignature(startIndex int, data string) (string, error) {
	addrBStartIndex := startIndex + len(trc20TransferMethodSignature)
	addrBEndIndex := addrBStartIndex
	if addrBEndIndex > len(data) {
		return "", errors.New("addrB not found in req")
	}

	addrB := data[addrBStartIndex : len(data)-64]
	addrB = removeLeadingZeros(addrB)

	decodedBytes, err := hex2.DecodeString(addrB)
	if err != nil {
		return "", fmt.Errorf("error while decoding addrB: %s", err)
	}
	if len(decodedBytes) == 20 {
		decodedBytes = append([]byte{address.TronBytePrefix}, decodedBytes...)
	}

	address := address.Address{}
	if err := address.Scan(decodedBytes); err != nil {
		return "", fmt.Errorf("error while scanning address %s", err)
	}
	return address.String(), nil
}

func removeLeadingZeros(str string) string {
	nonZeroIndex := 0
	for i, ch := range str {
		if ch != '0' {
			nonZeroIndex = i
			break
		}
	}

	return str[nonZeroIndex:]
}
