module gitlab.com/golib4/trc20decoder

go 1.22.2

require github.com/fbsobreira/gotron-sdk v0.0.0-20230907131216-1e824406fe8c

require (
	github.com/btcsuite/btcd/btcec/v2 v2.2.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	github.com/ethereum/go-ethereum v1.12.2 // indirect
	github.com/holiman/uint256 v1.2.3 // indirect
	github.com/shengdoushi/base58 v1.0.0 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
